package org.test.demo.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.test.demo.dto.ContactInformationCreateDto;
import org.test.demo.dto.UserCreateDto;
import org.test.demo.dto.UserDto;
import org.test.demo.mappers.ContactInformationMapper;
import org.test.demo.mappers.UserMapper;
import org.test.demo.models.ContactInformation;
import org.test.demo.models.User;
import org.test.demo.repository.ContactInformationRepository;
import org.test.demo.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserMapper userMapper;

    @Mock
    private ContactInformationMapper contactInformationMapper;

    @Mock
    private ContactInformationRepository contactInformationRepository;

    @InjectMocks
    private UserService userService;

    @Test
    public void addNewClientTest() {
        UserCreateDto createDto = new UserCreateDto();
        User user = new User();
        UserDto userDto = new UserDto();
        when(userMapper.createDtoToEntity(createDto)).thenReturn(user);
        when(userRepository.save(user)).thenReturn(user);
        when(userMapper.toDto(user)).thenReturn(userDto);

        UserDto result = userService.addNewClient(createDto);
        assertEquals(userDto, result);

        verify(userMapper).createDtoToEntity(createDto);
        verify(userMapper).toDto(user);
        verify(userRepository).save(user);
    }

    @Test
    public void addNewContactToClientTest() {
        Long userId = 1L;
        ContactInformationCreateDto contactDto = new ContactInformationCreateDto();
        User user = mock(User.class);
        ContactInformation contact = new ContactInformation();
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(contactInformationMapper.createDtoToEntity(contactDto)).thenReturn(contact);
        when(user.getContactInformation()).thenReturn(new ArrayList<>());

        userService.addNewContactToClient(userId, contactDto);

        verify(contactInformationMapper).createDtoToEntity(contactDto);
        verify(userRepository).save(user);
        assertTrue(user.getContactInformation().contains(contact));
    }
}