package org.test.demo.models;

public enum ContactType {

    EMAIL,
    PHONE

}
