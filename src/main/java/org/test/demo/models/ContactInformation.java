package org.test.demo.models;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@With
public class ContactInformation {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @NotNull(message = "contactType cannot be null")
    @Column(nullable = false)
    private ContactType contactType;
    @NotNull(message = "contact cannot be null")
    @NotEmpty(message = "contact cannot be empty")
    @Column(nullable = false)
    private String contact;

}
