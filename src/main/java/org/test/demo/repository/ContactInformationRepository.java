package org.test.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.test.demo.models.ContactInformation;
import org.test.demo.models.ContactType;

import java.util.List;
import java.util.Optional;

public interface ContactInformationRepository extends JpaRepository<ContactInformation, Long> {

    List<ContactInformation> findByUserIdAndContactType(Long userId, ContactType contactType);

}