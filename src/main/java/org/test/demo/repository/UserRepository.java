package org.test.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.test.demo.models.User;
public interface UserRepository extends JpaRepository<User, Long> {
}