package org.test.demo.service;

import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.test.demo.dto.ContactInformationCreateDto;
import org.test.demo.dto.ContactInformationDto;
import org.test.demo.dto.UserCreateDto;
import org.test.demo.dto.UserDto;
import org.test.demo.mappers.ContactInformationMapper;
import org.test.demo.mappers.UserMapper;
import org.test.demo.models.ContactInformation;
import org.test.demo.models.ContactType;
import org.test.demo.models.User;
import org.test.demo.repository.ContactInformationRepository;
import org.test.demo.repository.UserRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final ContactInformationMapper contactInformationMapper;
    private final ContactInformationRepository contactInformationRepository;

    public UserDto addNewClient(UserCreateDto createDto) {
        User user = userMapper.createDtoToEntity(createDto);
        user = userRepository.save(user);
        return userMapper.toDto(user);
    }

    @Transactional
    public void addNewContactToClient(Long userId, ContactInformationCreateDto contactInformationCreateDto) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException("User not found with id: " + userId));
        ContactInformation contact = contactInformationMapper.createDtoToEntity(contactInformationCreateDto);
        user.getContactInformation().add(contact);
        contactInformationRepository.save(contact.withUser(user));
        userRepository.save(user);
    }

    public List<UserDto> getAllClients() {
        return userRepository.findAll().stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }

    public UserDto getClientById(Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException("User not found with id: " + userId));
        return userMapper.toDto(user);
    }

    public List<ContactInformationDto> getContactsOfClient(Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException("User not found with id: " + userId));
        return user.getContactInformation().stream()
                .map(contactInformationMapper::toDto)
                .collect(Collectors.toList());
    }

    public List<ContactInformationDto> getContactsOfTypeOfClient(Long userId, ContactType contactType) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException("User not found with id: " + userId));
        return contactInformationRepository.findByUserIdAndContactType(user.getId(), contactType)
                .stream().map(contactInformationMapper::toDto)
                .collect(Collectors.toList());
    }

}
