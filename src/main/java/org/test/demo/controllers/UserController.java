package org.test.demo.controllers;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.test.demo.dto.ContactInformationCreateDto;
import org.test.demo.dto.ContactInformationDto;
import org.test.demo.dto.UserCreateDto;
import org.test.demo.dto.UserDto;
import org.test.demo.models.ContactType;
import org.test.demo.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @Operation(summary = "Добавляет нового клиента")
    @PostMapping
    public ResponseEntity<UserDto> addNewClient(@RequestBody UserCreateDto createDto) {
        UserDto newUser = userService.addNewClient(createDto);
        return new ResponseEntity<>(newUser, HttpStatus.CREATED);
    }

    @Operation(summary = "Добавляет новый контакт для клиента")
    @PostMapping("/{userId}/contacts")
    public ResponseEntity<Void> addNewContactToClient(@PathVariable Long userId,
                                                      @RequestBody ContactInformationCreateDto contactCreateDto) {
        userService.addNewContactToClient(userId, contactCreateDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Operation(summary = "Получает список всех клиентов")
    @GetMapping()
    public ResponseEntity<List<UserDto>> getAllClients() {
        List<UserDto> users = userService.getAllClients();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @Operation(summary = "Получает информацию о клиенте по его идентификатору")
    @GetMapping("/{userId}")
    public ResponseEntity<UserDto> getClientById(@PathVariable Long userId) {
        UserDto user = userService.getClientById(userId);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @Operation(summary = "Получает список контактов для заданного клиента")
    @GetMapping("/{userId}/contacts")
    public ResponseEntity<List<ContactInformationDto>> getContactsOfClient(@PathVariable Long userId) {
        List<ContactInformationDto> contacts = userService.getContactsOfClient(userId);
        return new ResponseEntity<>(contacts, HttpStatus.OK);
    }

    @Operation(summary = "Получает список контактов заданного типа для заданного клиента")
    @GetMapping("/{userId}/contacts/{contactType}")
    public ResponseEntity<List<ContactInformationDto>> getContactsOfTypeOfClient(@PathVariable Long userId,
                                                                                 @PathVariable ContactType contactType) {
        List<ContactInformationDto> contacts = userService.getContactsOfTypeOfClient(userId, contactType);
        return new ResponseEntity<>(contacts, HttpStatus.OK);
    }
}
