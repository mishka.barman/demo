package org.test.demo.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.test.demo.models.ContactInformation;
import org.test.demo.models.ContactType;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

/**
 * DTO for {@link ContactInformation}
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@With
public class ContactInformationDto {
    private Long id;
    @NotNull
    private ContactType contactType;
    @NotNull @NotEmpty
    private String contact;

    @Override
    public String toString() {
        return "ContactInformationDto[" +
                "id=" + id + ", " +
                "contactType=" + contactType + ", " +
                "contact=" + contact + ']';
    }

}