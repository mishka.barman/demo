package org.test.demo.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.test.demo.models.ContactType;

import java.io.Serializable;

/**
 * DTO for {@link org.test.demo.models.ContactInformation}
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@With
public class ContactInformationCreateDto {
    ContactType contactType;
    String contact;
}