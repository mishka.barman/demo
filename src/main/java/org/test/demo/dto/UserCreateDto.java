package org.test.demo.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.io.Serializable;

/**
 * DTO for {@link org.test.demo.models.User}
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@With
public class UserCreateDto {
    private String name;
}