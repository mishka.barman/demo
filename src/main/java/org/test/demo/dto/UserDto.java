package org.test.demo.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.test.demo.models.User;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * DTO for {@link User}
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@With
public class UserDto {

    Long id;
    @NotNull @NotEmpty
    String name;
    List<ContactInformationDto> contactInformation;
    LocalDateTime registeredAt;

}