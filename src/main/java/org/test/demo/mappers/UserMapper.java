package org.test.demo.mappers;

import org.mapstruct.*;
import org.test.demo.dto.UserCreateDto;
import org.test.demo.dto.UserDto;
import org.test.demo.models.User;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public interface UserMapper {
    User toEntity(UserDto userDto);

    User createDtoToEntity(UserCreateDto createDto);

    UserDto toDto(User user);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    User partialUpdate(UserDto userDto, @MappingTarget User user);
}