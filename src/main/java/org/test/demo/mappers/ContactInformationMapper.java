package org.test.demo.mappers;

import org.mapstruct.*;
import org.test.demo.dto.ContactInformationCreateDto;
import org.test.demo.dto.ContactInformationDto;
import org.test.demo.models.ContactInformation;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public interface ContactInformationMapper {
    ContactInformation toEntity(ContactInformationDto contactInformationDto);
    ContactInformation createDtoToEntity(ContactInformationCreateDto contactInformationDto);


    ContactInformationDto toDto(ContactInformation contactInformation);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    ContactInformation partialUpdate(ContactInformationDto contactInformationDto, @MappingTarget ContactInformation contactInformation);
}