CREATE SEQUENCE IF NOT EXISTS contact_information_seq START WITH 1 INCREMENT BY 50;

CREATE SEQUENCE IF NOT EXISTS usrs_seq START WITH 1 INCREMENT BY 50;

CREATE TABLE contact_information
(
    id           BIGINT       NOT NULL,
    user_id      BIGINT,
    contact_type SMALLINT     NOT NULL,
    contact      VARCHAR(255) NOT NULL,
    CONSTRAINT pk_contactinformation PRIMARY KEY (id)
);

CREATE TABLE usrs
(
    id            BIGINT       NOT NULL,
    name          VARCHAR(255) NOT NULL,
    registered_at TIMESTAMP WITHOUT TIME ZONE,
    CONSTRAINT pk_usrs PRIMARY KEY (id)
);

ALTER TABLE contact_information
    ADD CONSTRAINT FK_CONTACTINFORMATION_ON_USER FOREIGN KEY (user_id) REFERENCES usrs (id);